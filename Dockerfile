FROM centos/php-73-centos7

ENV NAME="OJS" \
    SUMMARY="Open Journal Systems" \
    DESCRIPTION="Open Journal Systems (OJS) is an open source software application for managing \
and publishing scholarly journals. Originally developed and released by PKP in \
2001 to improve access to research, it is the most widely used open source journal \
publishing platform in existence, with over 10,000 journals using it worldwide."

LABEL maintainer="CERN Publishing <publishing@cern.ch>" \
      summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      io.k8s.description="${DESCRIPTION}" \
      io.k8s.display-name="OJS" \
      io.openshift.tags="builder,${NAME}" \
      name="gitlab-registry.cern.ch/publishing/ojs/s2i-ojs-container" \
      version="${MKDOCS_VERSION}" \
      help="For more information visit https://cern.ch/publishing/" \
      usage="s2i build <SOURCE-REPOSITORY> gitlab-registry.cern.ch/publishing/ojs/s2i-ojs-container <APP-NAME>"

# Switch to the root user to install the necessary packages.
USER root

# Install this extension that uses the libmemcached library to provide an API
# for communicating with memcached servers.
RUN yum install -y --setopt=tsflags=nodocs sclo-php73-php-pecl-memcached --nogpgcheck && \
    rpm -V sclo-php73-php-pecl-memcached && \
    yum -y clean all --enablerepo='*'

# Switch back to the default user.
USER 1001